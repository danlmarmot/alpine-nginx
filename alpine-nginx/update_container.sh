#!/usr/bin/env bash

docker stop nginx

./build_container.sh
docker run -d -p 80:80 --rm --name nginx danlmarmot/alpine-linux
open http://localhost:80